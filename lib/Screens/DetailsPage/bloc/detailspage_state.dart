part of 'detailspage_bloc.dart';

@immutable
abstract class DetailspageState {}

class DetailspageInitialState extends DetailspageState {}

class DetailspageLoadingState extends DetailspageState {}

class DetailspageLoadedState extends DetailspageState {
  final Map<String, dynamic> resData;

  DetailspageLoadedState({this.resData});
}

class DetailspageErrorState extends DetailspageState {}
