import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:multi_bhashi_com/DataModels/PhotosDetails.dart';
import 'package:multi_bhashi_com/Network/Service.dart';
import 'package:multi_bhashi_com/Utils/HelperUtil.dart';

part 'detailspage_event.dart';
part 'detailspage_state.dart';

class DetailspageBloc extends Bloc<DetailspageEvent, DetailspageState> {
  DetailspageBloc() : super(DetailspageLoadingState());

  @override
  Stream<DetailspageState> mapEventToState(
    DetailspageEvent event,
  ) async* {
    if (event is GetPhotosDetailsEvent) {
      yield* _maptoGetPhotosDetailseventDatatoState(event);
    }
  }

  Stream<DetailspageState> _maptoGetPhotosDetailseventDatatoState(
      GetPhotosDetailsEvent event) async* {
    Map<String, dynamic> resData;

    try {
      getAlbumDetails() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await Service().getPhotosDetails(id: event.id).then((respObj) {
              resData = respObj;
            });
          }
        });
      }

      yield DetailspageLoadingState();
      await getAlbumDetails();
      yield DetailspageLoadedState(resData: resData);
    } catch (e) {
      yield DetailspageErrorState();
    }
  }
}
