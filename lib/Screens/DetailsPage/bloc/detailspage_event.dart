part of 'detailspage_bloc.dart';

@immutable
abstract class DetailspageEvent {}

class GetPhotosDetailsEvent extends DetailspageEvent {
  final int id;

  GetPhotosDetailsEvent({this.id});
}
