import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multi_bhashi_com/Screens/DetailsPage/bloc/detailspage_bloc.dart';
import 'package:multi_bhashi_com/Screens/DetailsPage/widgets/Appbar.dart';
import 'package:multi_bhashi_com/Screens/DetailsPage/widgets/DetailsPageShimmer.dart';
import 'package:multi_bhashi_com/Utils/ErrorWidget.dart';
import 'package:multi_bhashi_com/Utils/HelperUtil.dart';
import 'package:multi_bhashi_com/Utils/NoInternetUtil.dart';
import 'package:multi_bhashi_com/Utils/ToastUtil.dart';

import 'package:pinch_zoom/pinch_zoom.dart';

class DetailsPage extends StatefulWidget {
  final int id;
  final String name;

  const DetailsPage({Key key, this.id, this.name}) : super(key: key);
  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  final bloc = DetailspageBloc();
  bool isNetworkConnected = true;
  var _connectivitySubscription;

  bool isFullScreen = false;

  @override
  void initState() {
    super.initState();

    onNetworkChange();

    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        isNetworkConnected = true;
      } else {
        isNetworkConnected = false;
      }
    });
    bloc.add(GetPhotosDetailsEvent(id: widget.id));
  }

  @override
  void dispose() {
    bloc.close();
    _connectivitySubscription.cancel();
    super.dispose();
  }

  void onNetworkChange() {
    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        if (result == ConnectivityResult.none) {
          isNetworkConnected = false;
        } else {
          isNetworkConnected = true;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailspageBloc, DetailspageState>(
        cubit: bloc,
        buildWhen: (prevState, state) {
          if (state is DetailspageInitialState) {
            return false;
          }
        },
        builder: (context, state) {
          if (state is DetailspageLoadingState) {
            return WillPopScope(
              onWillPop: () {
                Navigator.pop(context);
              },
              child: SafeArea(
                child: Scaffold(
                  appBar: detailsPageAppBar(widget.name, context),
                  body: !isNetworkConnected
                      ? NoInternetUtil(retryInternetCallBack: callApiService)
                      : detailsPageShimmer(),
                ),
              ),
            );
          } else if (state is DetailspageLoadedState) {
            var path;
            if (state.resData['data']['thumbnail']['lqip']
                .toString()
                .contains('data:image/gif;base64,')) {
              path = state.resData['data']['thumbnail']['lqip']
                  .toString()
                  .split("data:image/gif;base64,");
            }

            return WillPopScope(
              onWillPop: () {
                Navigator.pop(context);
              },
              child: SafeArea(
                child: Scaffold(
                  appBar: detailsPageAppBar(widget.name, context),
                  body: !isNetworkConnected
                      ? NoInternetUtil(retryInternetCallBack: callApiService)
                      : Container(
                          child: Padding(
                            padding: isFullScreen
                                ? const EdgeInsets.all(0.0)
                                : const EdgeInsets.all(8.0),
                            child: SingleChildScrollView(
                              physics: isFullScreen
                                  ? NeverScrollableScrollPhysics()
                                  : AlwaysScrollableScrollPhysics(),
                              child: Column(
                                children: [
                                  Stack(
                                    children: [
                                      Container(
                                        height: isFullScreen
                                            ? MediaQuery.of(context).size.height
                                            : MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.30,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        color: Colors.blueGrey,
                                        child: PinchZoom(
                                          image: Image.memory(
                                            base64Decode(path[1]),
                                            fit: BoxFit.fill,
                                          ),
                                          zoomedBackgroundColor:
                                              Colors.black.withOpacity(0.5),
                                          resetDuration:
                                              const Duration(milliseconds: 100),
                                          maxScale: 2.5,
                                          onZoomStart: () {
                                            print('Start zooming');
                                          },
                                          onZoomEnd: () {
                                            print('Stop zooming');
                                          },
                                        ),
                                      ),
                                      Container(
                                        child: Positioned(
                                          right: 10.0,
                                          bottom: 10.0,
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.85,
                                          top: isFullScreen
                                              ? MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.70
                                              : MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.25,
                                          child: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                isFullScreen = !isFullScreen;
                                              });
                                            },
                                            child: Icon(
                                              isFullScreen
                                                  ? Icons.zoom_out
                                                  : Icons.zoom_in,
                                              size: 30,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  isFullScreen
                                      ? SizedBox(
                                          height: 0,
                                          width: 0,
                                        )
                                      : Padding(
                                          padding:
                                              const EdgeInsets.only(top: 8.0),
                                          child: Text(state.resData['data']
                                                      ['publication_history'] !=
                                                  null
                                              ? state.resData['data']
                                                      ['publication_history']
                                                  .toString()
                                              : " No Description found"),
                                        )
                                ],
                              ),
                            ),
                          ),
                        ),
                ),
              ),
            );
          } else {
            return WillPopScope(
              onWillPop: () {
                Navigator.pop(context);
              },
              child: Scaffold(
                appBar: detailsPageAppBar(widget.name, context),
                body: ErrorWidgetClass(
                  retryInternetCallBack: callApiService,
                ),
              ),
            );
          }
        });
  }

  void callApiService() {
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        bloc.add(GetPhotosDetailsEvent(id: widget.id));
      } else {
        ToastUtil().showMsg("no internet connection", Colors.black,
            Colors.white, 12.0, "short", "bottom");
      }
    });
  }
}
