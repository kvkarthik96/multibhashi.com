import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multi_bhashi_com/Resources/RouteConst.dart';
import 'package:multi_bhashi_com/Screens/Home/bloc/homepage_bloc.dart';
import 'package:multi_bhashi_com/Screens/Home/widgets/HomePageShimmer.dart';
import 'package:multi_bhashi_com/Screens/Home/widgets/appExitModel.dart';
import 'package:multi_bhashi_com/Utils/ErrorWidget.dart';
import 'package:multi_bhashi_com/Utils/HelperUtil.dart';
import 'package:multi_bhashi_com/Utils/NoInternetUtil.dart';
import 'package:multi_bhashi_com/Utils/ToastUtil.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomaPage extends StatefulWidget {
  @override
  _HomaPageState createState() => _HomaPageState();
}

class _HomaPageState extends State<HomaPage> {
  final homeBloc = HomepageBloc();
  bool isNetworkConnected = true;
  var _connectivitySubscription;
  String searchKey = "";
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  final TextEditingController searchController = new TextEditingController();
  final ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();

    onNetworkChange();

    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        isNetworkConnected = true;
      } else {
        isNetworkConnected = false;
      }
    });

    homeBloc.add(GetPhotosEvent(
        page: 1, searchKey: searchKey, data: [], showShimmer: true));
  }

  @override
  void dispose() {
    homeBloc.close();
    _connectivitySubscription.cancel();
    super.dispose();
  }

  void onNetworkChange() {
    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        if (result == ConnectivityResult.none) {
          isNetworkConnected = false;
        } else {
          isNetworkConnected = true;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomepageBloc, HomepageState>(
        cubit: homeBloc,
        buildWhen: (prevState, state) {
          if (state is HomepageInitialState) {
            return false;
          }
          // return false;
        },
        builder: (context, state) {
          if (state is HomepageLoadingState) {
            return WillPopScope(
              onWillPop: () {
                return openExitModel(context: context);
              },
              child: SafeArea(
                child: Scaffold(
                  appBar: PreferredSize(
                      child: Padding(
                        padding:
                            EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
                        child: Container(
                          width: 250,
                          height: 50,
                          child: Form(
                            child: Container(
                              decoration: new BoxDecoration(
                                color: const Color(0xffffffff),
                              ),
                              child: TextFormField(
                                readOnly: true,
                                controller: searchController,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.search,
                                maxLength: 50,
                                maxLengthEnforced: true,
                                onSaved: (String val) {},
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15.0,
                                ),
                                decoration: InputDecoration(
                                    hintText: "Search Items",
                                    focusColor: Colors.green,
                                    counterText: "",
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 1.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(9.0),
                                    ),
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Colors.grey),
                                    suffixIcon: IconButton(
                                      icon: Icon(Icons.search),
                                      color: Colors.black,
                                      onPressed: () {},
                                    )),
                              ),
                            ),
                          ),
                        ),
                      ),
                      preferredSize: Size.fromHeight(100)),
                  body: !isNetworkConnected
                      ? NoInternetUtil(retryInternetCallBack: callApiService)
                      : albumListShimmer(),
                ),
              ),
            );
          } else if (state is HomepageLoadedState) {
            return WillPopScope(
              onWillPop: () {
                return openExitModel(context: context);
              },
              child: SafeArea(
                child: Scaffold(
                  appBar: PreferredSize(
                      child: Padding(
                        padding:
                            EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
                        child: Container(
                          width: 250,
                          height: 50,
                          child: Form(
                            child: Container(
                              decoration: new BoxDecoration(
                                color: const Color(0xffffffff),
                              ),
                              child: TextFormField(
                                controller: searchController,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.search,
                                maxLength: 50,
                                maxLengthEnforced: true,
                                onFieldSubmitted: (text) {
                                  FocusScope.of(context).unfocus();
                                  onTextChanged(text);
                                },
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15.0,
                                ),
                                inputFormatters: [
                                  FilteringTextInputFormatter.deny(
                                      RegExp(r"\s\s")),
                                  FilteringTextInputFormatter.deny(RegExp(
                                      r'(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])')),
                                ],
                                decoration: InputDecoration(
                                    hintText: "Search Items",
                                    focusColor: Colors.green,
                                    counterText: "",
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 1.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(9.0),
                                    ),
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Colors.grey),
                                    suffixIcon: IconButton(
                                      icon: Icon(Icons.search),
                                      color: Colors.black,
                                      onPressed: () {
                                        FocusScope.of(context).unfocus();
                                        onTextChanged(searchController.text);
                                      },
                                    )),
                              ),
                            ),
                          ),
                        ),
                      ),
                      preferredSize: Size.fromHeight(100)),
                  // appBar: appBar(context, widget.name),
                  body: !isNetworkConnected
                      ? NoInternetUtil(retryInternetCallBack: callApiService)
                      : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: NotificationListener<ScrollEndNotification>(
                              onNotification: (ScrollNotification scrollState) {
                                FocusScope.of(context).unfocus();

                                if (!state.saving &&
                                    scrollState.metrics.pixels ==
                                        scrollState.metrics.maxScrollExtent) {
                                  if (state.resData != null &&
                                      state.resData.pagination != null &&
                                      state.resData.pagination.currentPage !=
                                          state.resData.pagination.totalPages) {
                                    state.saving = true;

                                    homeBloc.add(GetPhotosEvent(
                                        page: state.resData.pagination
                                                .currentPage +
                                            1,
                                        searchKey: searchKey,
                                        data: state.resData.data));
                                  }
                                }
                              },
                              child: SmartRefresher(
                                controller: _refreshController,
                                enablePullDown: false,
                                enablePullUp: (isNetworkConnected == false &&
                                        state.resData != null &&
                                        state.resData.pagination != null &&
                                        state.resData.pagination.currentPage ==
                                            state.resData.pagination.totalPages)
                                    ? false
                                    : true,
                                child: ListView.builder(
                                    controller: _scrollController,
                                    itemCount: state.resData.data.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Column(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              if (state.resData.data[index]
                                                          .thumbnail ==
                                                      null ||
                                                  state.resData.data[index]
                                                          .thumbnail.lqip ==
                                                      null) {
                                                ToastUtil().showMsg(
                                                    "No image data found",
                                                    Colors.black,
                                                    Colors.white,
                                                    12.0,
                                                    "short",
                                                    "centre");
                                              } else {
                                                Map<String, dynamic> data = {
                                                  "id": state
                                                      .resData.data[index].id,
                                                  "name": state
                                                      .resData.data[index].title
                                                };

                                                Navigator.of(context).pushNamed(
                                                    RouteConst.routeDetailsPage,
                                                    arguments: {"data": data});
                                              }
                                            },
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Container(
                                                color: Colors.grey[300],
                                                child: Stack(
                                                  children: [
                                                    new ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  2.0)),
                                                      child: Container(
                                                        child: Container(
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          child: displayImage(state
                                                                      .resData
                                                                      .data[
                                                                          index]
                                                                      .thumbnail !=
                                                                  null
                                                              ? state
                                                                  .resData
                                                                  .data[index]
                                                                  .thumbnail
                                                                  .lqip
                                                                  .toString()
                                                              : ""),
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 160.0,
                                                              left: 8.0,
                                                              right: 8.0,
                                                              bottom: 4.0),
                                                      child: Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Text(
                                                          state
                                                              .resData
                                                              .data[index]
                                                              .title,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 2,
                                                          textAlign:
                                                              TextAlign.start,
                                                          style: TextStyle(
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            fontStyle: FontStyle
                                                                .normal,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 8.0, right: 8.0),
                                            child: Divider(
                                              color: Colors.grey,
                                            ),
                                          )
                                        ],
                                      );
                                    }),
                              ),
                            ),
                          ),
                        ),
                ),
              ),
            );
          } else {
            return WillPopScope(
              onWillPop: () {
                return openExitModel(context: context);
              },
              child: Scaffold(
                //  appBar: appBar(context, widget.name),
                body: ErrorWidgetClass(
                  retryInternetCallBack: callApiService,
                ),
              ),
            );
          }
        });
  }

  void callApiService() {
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        homeBloc.add(GetPhotosEvent(
            page: 1, searchKey: searchKey, data: [], showShimmer: true));
      } else {
        ToastUtil().showMsg("no internet connection", Colors.black,
            Colors.white, 12.0, "short", "bottom");
      }
    });
  }

  void onTextChanged(String text) {
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        homeBloc.add(GetPhotosEvent(
            page: 1, searchKey: text, data: [], showShimmer: true));
      } else {}
    });
  }

  Widget displayImage(String imagePath) {
    var path;
    if (imagePath != null && imagePath.contains('data:image/gif;base64,')) {
      path = imagePath.split("data:image/gif;base64,");
      return Container(
          // color: Colors.grey[300],
          height: 200,
          width: MediaQuery.of(context).size.width,
          child: Image.memory(
            base64Decode(path[1]),
            fit: BoxFit.fill,
          ));
    } else {
      return Container(
        //color: Colors.grey[300],
        height: 200,
        width: MediaQuery.of(context).size.width,
      );
    }
  }
}
