import 'dart:io';

import 'package:flutter/material.dart';

openExitModel({BuildContext context}) {
  showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text("Are you really want to exit?"),
          actions: <Widget>[
            FlatButton(
              child: new Text(
                "Yes",
                style: TextStyle(color: Colors.blue),
                semanticsLabel: 'Yes',
              ),
              onPressed: () {
                exit(0);
              },
            ),
            FlatButton(
              child: new Text(
                "No",
                style: TextStyle(color: Colors.blue),
                semanticsLabel: 'No',
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      });
}
