import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:multi_bhashi_com/DataModels/PhotosList.dart';
import 'package:multi_bhashi_com/Network/Service.dart';
import 'package:multi_bhashi_com/Utils/HelperUtil.dart';

part 'homepage_event.dart';
part 'homepage_state.dart';

class HomepageBloc extends Bloc<HomepageEvent, HomepageState> {
  HomepageBloc() : super(HomepageLoadingState());

  @override
  Stream<HomepageState> mapEventToState(
    HomepageEvent event,
  ) async* {
    if (event is GetPhotosEvent) {
      yield* _mapGetPhotosEventDatatoState(event);
    }
  }

  Stream<HomepageState> _mapGetPhotosEventDatatoState(
      GetPhotosEvent event) async* {
    PhotosList resData;
    List<Data> prevData = event.data;
    bool saving = true;

    try {
      getAlbumDetails() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await Service()
                .getPhotosList(page: event.page, searchKey: event.searchKey)
                .then((respObj) {
              print(respObj);
              resData = respObj;
              if (resData.data != null && resData.data.length > 0) {
                if (prevData != null && prevData.length > 0) {
                  prevData.addAll(resData.data);
                  resData.data = event.data;
                }
              } else {
                resData.data = event.data;
              }
              saving = false;
            });
          }
        });
      }

      if (event.page == 1 && event.showShimmer == true) {
        yield HomepageLoadingState();
      } else {
        yield HomepageInitialState();
      }
      await getAlbumDetails();
      yield HomepageLoadedState(resData: resData, saving: saving);
    } catch (e) {
      yield HomepageErrorState();
    }
  }
}
