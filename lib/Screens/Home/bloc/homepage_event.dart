part of 'homepage_bloc.dart';

@immutable
abstract class HomepageEvent {}

class GetPhotosEvent extends HomepageEvent {
  final int page;
  final String searchKey;
  final bool saving;
  List<Data> data = [];
  final showShimmer;
  GetPhotosEvent(
      {this.page,
      this.searchKey,
      this.saving,
      this.data,
      this.showShimmer = false});
}
