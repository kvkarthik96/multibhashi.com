import 'package:flutter/material.dart';
import 'package:multi_bhashi_com/Routers/Locator.dart';

import 'Resources/RouteConst.dart';
import 'Routers/NavigatorService.dart';
import 'Routers/Routing.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "MuiltBhashi.com",
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        brightness: Brightness.light,
      ),
      initialRoute: RouteConst.routeDefault,
      onGenerateRoute: RouteGenerator.generateRoute,
      navigatorKey: locator<NavigationService>().navigatorKey,
      debugShowCheckedModeBanner: false,
    );
  }
}
