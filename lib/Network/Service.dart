import 'dart:async';
import 'dart:core';

import 'package:multi_bhashi_com/DataModels/PhotosList.dart';
import 'package:multi_bhashi_com/Network/Network.dart';

class Service {
  Network _network = new Network();

  Map<String, String> data = new Map<String, String>();

  Future<PhotosList> getPhotosList({int page, String searchKey}) {
    data['Content-Type'] = 'application/json';
    return _network
        .get(
            'https://api.artic.edu/api/v1/artworks/search?q=$searchKey&page=$page',
            headers: data)
        .then((dynamic res) {
      return new PhotosList.fromJson(res);
    });
  }

  Future<Map<String, dynamic>> getPhotosDetails({int id}) {
    data['Content-Type'] = 'application/json';
    return _network
        .get('https://api.artic.edu/api/v1/artworks/$id', headers: data)
        .then((dynamic res) {
      return res;
    });
  }
}
